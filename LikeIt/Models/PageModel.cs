﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeIt.Models
{
    public class PageModel
    {
        public Int32 Id { get; set; }
        public String Text { get; set; }      
        public ICollection<NiceEvent> NiceEvents { get; set; }
    }
}