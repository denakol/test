﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LikeIt.Models
{
    public class NiceEvent
    {
        public Int32 Id { get; set; }

        [JsonProperty("pageId")]
        [ForeignKey("Page")]
        public Int32 PageId { get; set; }

        public DateTime Created { get; set; }

        public PageModel Page { get; set; }
    }
}