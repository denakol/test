﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LikeIt.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var currentDomain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
                             (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
            ViewBag.PagesUrl = String.Format("{0}/pages", currentDomain);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}