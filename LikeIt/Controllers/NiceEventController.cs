﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using LikeIt.Models;
using LikeIt.DB;

namespace LikeIt.Controllers
{
    public class NiceEventController : ApiController
    {
        private LikeItContext db = new LikeItContext();

        [ResponseType(typeof(NiceEvent))]
        public IHttpActionResult PostNiceEvent(NiceEvent niceevent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            niceevent.Created = DateTime.Now;
            db.NiceEvents.Add(niceevent);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = niceevent.Id }, niceevent);
        }

        /// <summary>
        /// Delete NiceEvent by PageId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ResponseType(typeof(NiceEvent))]
        public IHttpActionResult DeleteNiceEvent(int id)
        {
            NiceEvent niceevent = db.NiceEvents.FirstOrDefault(x => x.PageId == id);
            if (niceevent == null)
            {
                return NotFound();
            }

            db.NiceEvents.Remove(niceevent);
            db.SaveChanges();

            return Ok(niceevent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}