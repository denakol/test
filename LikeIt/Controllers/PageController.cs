﻿using LikeIt.DB;
using LikeIt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LikeIt.Controllers
{
    public class PagesController : Controller
    {
        //
        // GET: /Page/
        public ActionResult Index()
        {
            var currentDomain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
                             (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
            ViewBag.UrlPages = String.Format("{0}/pages/page/", currentDomain);
            return View("Index", (new LikeItContext()).Pages.ToList());
        }

        public ActionResult Page(Int32 id)
        {
            PageModel item;
            using (var db = new LikeItContext())
            {
                item = db.Pages.FirstOrDefault(x => x.Id == id);
            }
            if (item == null)
            {
                return View("Error", new ErrorModel() { Text = "Страница не найдена" });
            }
            return View("Template", item);
        }
    }
}