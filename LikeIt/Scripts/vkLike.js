﻿'use strict'
$(function () {
    var url = location.protocol + "//" + location.host;
    var urlNiceEvent = url + "/api/NiceEvent/";
    var pageId = $("#pageId").val();
    VK.init({
        apiId: 4241702,
        onlyWidgets: true
    });
    VK.Widgets.Like("vk_like", { type: "button" });
    VK.Observer.subscribe("widgets.like.liked", function f() {
        $.post(urlNiceEvent, { pageId: pageId })
          .done(function (data) {
              alert("Вам понравилась эта страница)");
          });
    });


    VK.Observer.subscribe("widgets.like.unliked", function f() {
        $.ajax({
            type: "DELETE",
            url: urlNiceEvent + pageId,
            success: function (data) {
                alert("Вам уже не нравится эта страница");
            }
        });

    });

});