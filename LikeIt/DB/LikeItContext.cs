﻿using LikeIt.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LikeIt.DB
{
    public class LikeItContext : DbContext
    {
        public DbSet<NiceEvent> NiceEvents { get; set; }
        public DbSet<PageModel> Pages { get; set; }
    }
}