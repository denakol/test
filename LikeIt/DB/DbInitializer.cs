﻿using LikeIt.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LikeIt.DB
{
    public class DbInitializer : DropCreateDatabaseIfModelChanges<LikeItContext>
    {
        protected override void Seed(LikeItContext context)
        {
            for (var i = 0; i < 10; i++)
            {
                context.Pages.Add(new PageModel() { Text = String.Format("Привет я страница номер {0}", i) });
            }
            context.SaveChanges();
        }
    }
}